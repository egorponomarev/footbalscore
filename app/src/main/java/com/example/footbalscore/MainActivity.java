package com.example.footbalscore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer counterRus = 0;
    private Integer counterFrance = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView counterViewFrance = findViewById(R.id.counterFrance);
        TextView counterViewRus = findViewById(R.id.counterRus);
        if (savedInstanceState != null) {
            counterViewFrance.setText(savedInstanceState.getInt("keyOne") + "");
            counterViewRus.setText(savedInstanceState.getInt("keyTwo") +"");
        }
    }
    public void addFrance(View view) {
        counterFrance++;
        TextView counterView = findViewById(R.id.counterFrance);
        counterView.setText(counterFrance +"");
    }
    public void addRus(View view){
        counterRus++;
        TextView counterViev = findViewById(R.id.counterRus);
        counterViev.setText(counterRus+"");
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("keyOne", counterFrance);
        outState.putInt("keyTwo", counterRus);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        counterFrance = savedInstanceState.getInt("keyOne");
        counterRus = savedInstanceState.getInt("keyTwo");
    }
}
